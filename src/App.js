import React from "react";
import faker from "faker";
import logo from "./logo.svg";
import "./App.css";
import "antd/dist/antd.min.css";

import { Layout, Menu, Breadcrumb } from "antd";
import Actu from "./Actu";

const { Header, Content, Footer } = Layout;

const test = [0, 1, 2, 3, 4, 5];

function App() {
  return (
    <div className="App">
      <Layout className="layout" style={{ display: "flex", height: "100%" }}>
        <Header>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["1"]}
            style={{ lineHeight: "64px" }}
          >
            <Menu.Item key="1">Instafuck</Menu.Item>
          </Menu>
        </Header>
        <Content
          style={{ padding: "0 50px", display: "flex", flexFlow: "column" }}
        >
          <Breadcrumb style={{ margin: "16px 0" }}>
            <Breadcrumb.Item>Actualités</Breadcrumb.Item>
          </Breadcrumb>
          <div
            style={{
              alignItems: "center",
              background: "#fff",
              display: "flex",
              overflowY: "auto",
              flex: "1 1 auto",
              flexAlign: "center",
              flexFlow: "column",
              padding: 24,
              minHeight: 280
            }}
          >
            {test.map(() => {
              const firstName = faker.name.firstName();
              const avatar = faker.image.avatar();
              const url = faker.internet.avatar();
              return <Actu name={firstName} avatar={avatar} url={url} />;
            })}
            <Actu />
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>Instafuck</Footer>
      </Layout>
    </div>
  );
}

export default App;
